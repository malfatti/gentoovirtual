# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v3

EAPI=8
PYTHON_COMPAT=( python3_{8..10} )

inherit python-r1

DESCRIPTION="Python port of the UniDip clustering algorithm"
HOMEPAGE="http://github.com/BenjaminDoran/unidip"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
REQUIRED_USE="${PYTHON_REQUIRED_USE}"

RDEPEND="
	$(python_gen_cond_dep 'dev-python/numpy[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'dev-python/matplotlib[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'dev-python/cycler[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'dev-python/fonttools[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'dev-python/kiwisolver[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'dev-python/packaging[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'dev-python/pillow[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'dev-python/pyparsing[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'dev-python/python-dateutil[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'dev-python/setuptools_scm[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'dev-python/six[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'dev-python/setuptools[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'dev-python/tomli[${PYTHON_USEDEP}]' 'python*')
"
DEPEND="${RDEPEND}"
