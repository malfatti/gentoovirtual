# Copyright 2022-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v3

EAPI=8
PYTHON_COMPAT=( python3_{8..13} )

inherit python-r1

DESCRIPTION="Command-line tool to manage bibliography (pdfs + bibtex)"
HOMEPAGE="https://github.com/perrette/papers"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
REQUIRED_USE="${PYTHON_REQUIRED_USE}"
IUSE="doc test"

# Missing:
	# dev-python/crossrefapi[${PYTHON_USEDEP}]
	# dev-python/scholarly[${PYTHON_USEDEP}]
RDEPEND="
	$(python_gen_cond_dep 'dev-python/bibtexparser[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'dev-python/fuzzywuzzy[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'dev-python/six[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'dev-python/unidecode[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'dev-python/chardet[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'dev-python/pyicu[${PYTHON_USEDEP}]' 'python*')
"
DEPEND="${RDEPEND}
	$(python_gen_cond_dep 'dev-python/setuptools[${PYTHON_USEDEP}]' 'python*')
	$(python_gen_cond_dep 'doc? ( dev-python/sphinx[${PYTHON_USEDEP}] )' 'python*')
	$(python_gen_cond_dep 'test? ( dev-python/pytest[${PYTHON_USEDEP}] )' 'python*')
"
