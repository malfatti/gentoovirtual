# GentooVirtual

Virtual ebuilds for gentoo. These are useful for when software that is not on tree needs to be installed and (at least some of) its dependencies dependencies are on tree. Using virtual ebuilds keep the world file clean, specially when (if ever) said software is added to the main tree. Ideally, this repository should be empty :)

